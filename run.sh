#!/bin/bash
# ========================================================================== #
# Copyright (C) 2019  Rajan Gill
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
# ========================================================================== #

if test -e "log"; then
	rm log/*
else
	mkdir "log"
fi
# make sure command line arguments are passed to the script
if [ $# -eq 0 ]
then
	echo "Usage: $0 number"
	exit 1
fi

# use for loop
n=$1
echo "running for $n times"
for ((i=0; i<$n; i++))
do
	./main.out
done
