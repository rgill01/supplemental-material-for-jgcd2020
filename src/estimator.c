/****************************************************************************
  estimators.c
  Copyright (C) 2019  Rajan Gill

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "estimators.h"
#include <utilities.h>
#include <stdio.h>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>

/****************************************************************************
 * Pre-processor
 ****************************************************************************/

#define MEKF_DT       0.001 /* in paper, used 0.001 and 0.00001 */
#define UKF_ALPHA     (1)
#define UKF_BETA      (2)   
#define N 9                 /* convenience for UKF sigma points */

// #define USE_JOSEPH_FORM

/****************************************************************************
 * Private Function Definitions
 ****************************************************************************/

static void est_common_attitude_reset(est_s *est, const vec3d *delta, const rotd *exp_d)
{
  rotd_assign_by_product(&est->state.R, exp_d);
  rotd_normalize(&est->state.R);

  mat3d G;
  est->Gamma(delta, &G);
  mat9d T = {
    {1, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 1, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, G[0][0], G[0][1], G[0][2]},
    {0, 0, 0, 0, 0, 0, G[1][0], G[1][1], G[1][2]},
    {0, 0, 0, 0, 0, 0, G[2][0], G[2][1], G[2][2]},
  };
  mat9d P_old;
  mat9d_copy(&est->P, &P_old);
  mat9d TP;
  mat9d_mult(&T, &est->P, &TP);
  mat9d_mult_transpose(&TP, &T, &est->P);

  /* symmetrize */
  mat9d PT;
  mat9d_transpose(&est->P, &PT);
  mat9d_add(&est->P, &PT, &est->P);
  mat9d_mult_scalar(&est->P, 0.5, &est->P);
}

/*****************************************************************************
 * Name: est_scalar_update_correction
 *
 * Description:
 *   Helper function. Update covariance but not the state. Get the correction.
 *   @a : cross cov x and y, P*C^T, 
 *          make sure this doesn't point to somewhere in g_est.cov!!!
 *   @sy : C*P*C^T + r, output variance
 *   @e : scalar error
 *   @dx : k*e
 * Return:
 *
 *****************************************************************************/

static inline void est_scalar_update_correction(const vec9d *Sxy, double sy, 
  double e, vec9d *dx, mat9d *P)
{
  uint8_t j,k; 

  /* error term */
  vec9d K;
  vec9d_div_scalar(Sxy, sy, &K);
  vec9d_mult_scalar(&K, e, dx);

  /* covariance update. P <- P - KCP = P - Ka^T */
  for (j=0; j<9; j++) {
    (*P)[j][j] = (*P)[j][j] - K[j]*(*Sxy)[j]; /* diagonal*/
    for (k=j+1; k<9; k++) {   /* offdiagonal, symmetric..*/
      (*P)[k][j] = (*P)[j][k] = (*P)[j][k] - K[j]*(*Sxy)[k];
    }
  }
}

static inline void est_scalar_update_correction_joseph(const vec9d *Sxy, double sy, 
  double e, vec9d *dx, mat9d *P, const vec9d *C, double r)
{
  /* error term */
  vec9d K;
  vec9d_div_scalar(Sxy, sy, &K);
  vec9d_mult_scalar(&K, e, dx);

  /* covariance update. P <- P - KCP = P - Ka^T */
  mat9d KC, ImKC, rKKt;
  mat9d_outer(&K, &K, &rKKt);
  mat9d_mult_scalar(&rKKt, r, &rKKt);
  mat9d_outer(&K, C, &KC);
  mat9d I;
  mat9d_diag(&I, 1);
  mat9d_subtract(&I, &KC, &ImKC);

  mat9d temp;
  mat9d_mult(&ImKC, P, &temp);
  mat9d_mult_transpose(&temp, &ImKC, P);
  mat9d_add(P, &rKKt, P);
}

/****************************************************************************
 * Public Function Definitions
 ****************************************************************************/

/****************************************************************************
 * Gammas 
 ****************************************************************************/

void zero_order_gamma(const vec3d *v, mat3d *Gamma)
{
  mat3d_copy(&mat3d_identity, Gamma);
}

void first_order_gamma(const vec3d *v, mat3d *Gamma)
{
  vec3d n_half_mu;

  vec3d_div_scalar(v, -2, &n_half_mu);
  mat3d_skew(&n_half_mu, Gamma);
  (*Gamma)[0][0] += 1;
  (*Gamma)[1][1] += 1;
  (*Gamma)[2][2] += 1;
}

void full_order_gamma(const vec3d *v, mat3d *Gamma)
{
  double psi = vec3d_norm(v);
  if (psi > 1e-10) {
    mat3d v_skew, v_outer;
    mat3d_skew(v, &v_skew);
    mat3d_outer(v, v, &v_outer);

    mat3d_mult_scalar(&v_skew,  (cos(psi)-1)/squared(psi), &v_skew);
    mat3d_mult_scalar(&v_outer, (psi-sin(psi))/cubed(psi),  &v_outer);
    mat3d_add(&v_skew, &v_outer, Gamma);
    (*Gamma)[0][0] += sin(psi)/psi;
    (*Gamma)[1][1] += sin(psi)/psi;
    (*Gamma)[2][2] += sin(psi)/psi;
  } else {
    mat3d_diag(Gamma, 1);
  }
}

void full_order_gamma_gibbs(const vec3d *v, mat3d *Gamma)
{
  first_order_gamma(v, Gamma);
  mat3d_div_scalar(Gamma, 1+vec3d_norm_sq(v)/4, Gamma);
}

void full_order_gamma_quaternion(const vec3d *v, mat3d *Gamma)
{
  mat3d v_skew, v_skew_sq;
  mat3d_skew(v, &v_skew);
  mat3d_mult(&v_skew, &v_skew, &v_skew_sq);
  mat3d_div_scalar(&v_skew_sq, 4, &v_skew_sq);
  mat3d_add(&mat3d_identity, &v_skew_sq, Gamma); 
  mat3d_div_scalar(Gamma, sqrt(1-vec3d_norm_sq(v)/4), Gamma);
  mat3d_div_scalar(&v_skew, 2, &v_skew);
  mat3d_subtract(Gamma, &v_skew, Gamma);
}

/****************************************************************************
 * Explicit attitude resets 
 ****************************************************************************/

void mekf_attitude_reset(est_s *est, const vec3d *delta)
{
  if (est->Gamma == full_order_gamma_gibbs) {
    usque_attitude_reset(est, delta);
  } else if (est->Gamma == full_order_gamma_quaternion) {
    vec3d temp;
    rotd exp_d;
    vec3d_div_scalar(delta, 2, &temp);
    rotd_from_vector_part(&temp, &exp_d);
    est_common_attitude_reset(est, delta, &exp_d); 
  } else {
    reset_attitude_reset(est, delta);
  }  
}

void reset_attitude_reset(est_s *est, const vec3d *delta)
{
  rotd exp_d;
    
  /* we assume rotation vector */
  rotd_from_rotation_vector(delta, &exp_d);
  rotd_normalize(&exp_d);

  est_common_attitude_reset(est, delta, &exp_d);
}

void usque_attitude_reset(est_s *est, const vec3d *delta)
{
  rotd exp_d;

  /* usque assumes generalized rodrigues, use f=2,a=0 */
  exp_d[0] = 2*sqrt(4 + vec3d_norm_sq(delta))/(4 + vec3d_norm_sq(delta));
  vec3d_mult_scalar(delta, exp_d[0]/2, (vec3d *) &exp_d[1]);
  rotd_normalize(&exp_d);

  est_common_attitude_reset(est, delta, &exp_d);
}

/****************************************************************************
 * EKFs
 ****************************************************************************/

void ekf_euler_reset_predict(est_s *est, double DT, const vec3d *Ba, const vec3d *Bg)
{
  /* mean push */
  vec3d Ia;
  rotd_body2inertial(&est->state.R, Ba, &Ia);

  vec3d_add_s_mult(&est->state.pos, &est->state.vel, DT, &est->state.pos);
  vec3d_add_s_mult(&est->state.pos, &Ia, squared(DT)/2, &est->state.pos);

  vec3d_add_s_mult(&est->state.vel, &Ia, DT, &est->state.vel);

  vec3d delta;
  vec3d_mult_scalar(Bg, DT, &delta);

  /* cov push */
  mat3d acc_skew = {{0}};
  mat3d R1;         /* -Rref[acc x]dt */
  mat3d R2;         /* -Rref[acc x]dt^2/2 */
  mat3d G2 = {{0}}; /* -1/2[gyro x]dt */
  mat3d Rref;

  rotd_rotation_matrix(&est->state.R, &Rref);
  mat3d_skew(Ba, &acc_skew);
  mat3d_skew(Bg, &G2);

  mat3d_mult(&Rref, &acc_skew, &R1);
  mat3d_mult_scalar(&R1, -DT, &R1);
  mat3d_mult_scalar(&R1, DT/2, &R2);

  mat3d_mult_scalar(&G2, -DT/2, &G2);

  mat9d Ad = {
    {1, 0, 0, DT, 0,  0,  R2[0][0],     R2[0][1],   R2[0][2]},
    {0, 1, 0, 0,  DT, 0,  R2[1][0],     R2[1][1],   R2[1][2]},
    {0, 0, 1, 0,  0,  DT, R2[2][0],     R2[2][1],   R2[2][2]},
    {0, 0, 0, 1,  0,  0,  R1[0][0],     R1[0][1],   R1[0][2]},
    {0, 0, 0, 0,  1,  0,  R1[1][0],     R1[1][1],   R1[1][2]},
    {0, 0, 0, 0,  0,  1,  R1[2][0],     R1[2][1],   R1[2][2]},
    {0, 0, 0, 0,  0,  0,  1+G2[0][0],   G2[0][1],   G2[0][2]},
    {0, 0, 0, 0,  0,  0,  G2[1][0],     1+G2[1][1], G2[1][2]},
    {0, 0, 0, 0,  0,  0,  G2[2][0],     G2[2][1],   1+G2[2][2]},  
  };

  mat9d AdP;
  mat9d_mult(&Ad, &est->P, &AdP);
  mat9d_mult_transpose(&AdP, &Ad, &est->P);

  /* add process noise */
  mat9d Q = {{0}};
  for (int j=0; j<3; j++) {
    Q[j][j]     = squared(DT)*squared(DT)/4*est->q_acc;
    Q[j][j+3]   = Q[j+3][j] = squared(DT)*DT/2*est->q_acc;
    Q[j+3][j+3] = squared(DT)*est->q_acc;
    Q[j+6][j+6] = squared(DT)*est->q_gyro;
  } 
  mat9d_add(&est->P, &Q, &est->P);

  reset_attitude_reset(est, &delta);
}

void ekf_euler_reset_1_predict(est_s *est, double DT, const vec3d *Ba, const vec3d *Bg)
{
  /* mean push */
  vec3d Ia;
  rotd_body2inertial(&est->state.R, Ba, &Ia);

  vec3d_add_s_mult(&est->state.pos, &est->state.vel, DT, &est->state.pos);
  vec3d_add_s_mult(&est->state.pos, &Ia, squared(DT)/2, &est->state.pos);

  vec3d_add_s_mult(&est->state.vel, &Ia, DT, &est->state.vel);

  vec3d delta;
  vec3d_mult_scalar(Bg, DT, &delta);

  /* cov push */
  mat3d acc_skew = {{0}};
  mat3d R1;         /* -Rref[acc x]dt */
  mat3d R2;         /* -Rref[acc x]dt^2/2 */
  mat3d G2 = {{0}}; /* -1/2[gyro x]dt */
  mat3d Rref;

  rotd_rotation_matrix(&est->state.R, &Rref);
  mat3d_skew(Ba, &acc_skew);
  mat3d_skew(Bg, &G2);

  mat3d_mult(&Rref, &acc_skew, &R1);
  mat3d_mult_scalar(&R1, -DT, &R1);
  mat3d_mult_scalar(&R1, DT/2, &R2);

  mat3d_mult_scalar(&G2, -DT/2, &G2);

  mat9d Ad = {
    {1, 0, 0, DT, 0,  0,  R2[0][0],     R2[0][1],   R2[0][2]},
    {0, 1, 0, 0,  DT, 0,  R2[1][0],     R2[1][1],   R2[1][2]},
    {0, 0, 1, 0,  0,  DT, R2[2][0],     R2[2][1],   R2[2][2]},
    {0, 0, 0, 1,  0,  0,  R1[0][0],     R1[0][1],   R1[0][2]},
    {0, 0, 0, 0,  1,  0,  R1[1][0],     R1[1][1],   R1[1][2]},
    {0, 0, 0, 0,  0,  1,  R1[2][0],     R1[2][1],   R1[2][2]},
    {0, 0, 0, 0,  0,  0,  1+G2[0][0],   G2[0][1],   G2[0][2]},
    {0, 0, 0, 0,  0,  0,  G2[1][0],     1+G2[1][1], G2[1][2]},
    {0, 0, 0, 0,  0,  0,  G2[2][0],     G2[2][1],   1+G2[2][2]},  
  };

  mat9d AdP;
  mat9d_mult(&Ad, &est->P, &AdP);
  mat9d_mult_transpose(&AdP, &Ad, &est->P);

  /* add process noise */
  mat9d Q = {{0}};
  for (int j=0; j<3; j++) {
    Q[j][j]     = squared(DT)*squared(DT)/4*est->q_acc;
    Q[j][j+3]   = Q[j+3][j] = squared(DT)*DT/2*est->q_acc;
    Q[j+3][j+3] = squared(DT)*est->q_acc;
    Q[j+6][j+6] = squared(DT)*est->q_gyro;
  } 
  mat9d_add(&est->P, &Q, &est->P);

  /* hacky..this function uses a first order reset in the prediction, 
   * whatever order (saved in tmp) in update step
   */
  void (*tmp)() = est->Gamma;
  est->Gamma = first_order_gamma;
  reset_attitude_reset(est, &delta);
  est->Gamma = tmp;
}

void gps_update(est_s *est, const vec3d *p, vec9d *corrections)
{
  /* do scalar updates */
  vec9d xtilde = {0};
  for (int i=0; i<3; i++) {
    vec9d Sxy;
    vec9d_copy(&est->P[i], &Sxy);

    double Syy = est->P[i][i] + est->r;

    vec9d dx; 
#ifndef USE_JOSEPH_FORM    
    est_scalar_update_correction(&Sxy, Syy, (*p)[i] - est->state.pos[i] - xtilde[i], 
      &dx, &est->P);
#else
    vec9d C = {0};
    C[i] = 1;
    est_scalar_update_correction_joseph(&Sxy, Syy, (*p)[i] - est->state.pos[i] - xtilde[i], 
      &dx, &est->P, &C, est->r);
#endif
    vec9d_add(&dx, &xtilde, &xtilde);
  }

  vec3d_add((const vec3d *) &xtilde, &est->state.pos, &est->state.pos);      
  vec3d_add((const vec3d *) &xtilde[3], &est->state.vel, &est->state.vel);
  est->attitude_reset(est, (const vec3d *) &xtilde[6]);

  vec9d_copy(&xtilde, corrections);
}

void mekf_predict(est_s *est, double DT, const vec3d *Ba, const vec3d *Bg)
{
  /* mean push */
  vec3d Ia;
  rotd_body2inertial(&est->state.R, Ba, &Ia);

  vec3d_add_s_mult(&est->state.pos, &est->state.vel, DT, &est->state.pos);
  vec3d_add_s_mult(&est->state.pos, &Ia, squared(DT)/2, &est->state.pos);

  vec3d_add_s_mult(&est->state.vel, &Ia, DT, &est->state.vel);

  /* Reference attitude evolves with the covariance */

  /* cov push */
  for (double t = 0; t < DT; t += MEKF_DT) {
    mat3d acc_skew = {{0}};
    mat3d R1;         /* -Rref exp(w dt) [acc x] */
    mat3d G2 = {{0}}; /* -[gyro x] */
    mat3d Rref;

    /* Reference attitude evolves with the covariance */
    vec3d delta;
    vec3d_mult_scalar(Bg, MEKF_DT, &delta);
    rotd exp_delta;
    rotd_from_rotation_vector(&delta, &exp_delta);
    
    rotd_rotation_matrix(&est->state.R, &Rref);
    mat3d_skew(Ba, &acc_skew);
    mat3d_skew(Bg, &G2);

    mat3d_mult(&Rref, &acc_skew, &R1);
    mat3d_mult_scalar(&R1, -1, &R1);
    mat3d_mult_scalar(&G2, -1, &G2);
    mat9d F = {
      {0, 0, 0, 1,  0,  0,  0,     0,   0},
      {0, 0, 0, 0,  1,  0,  0,     0,   0},
      {0, 0, 0, 0,  0,  1,  0,     0,   0},
      {0, 0, 0, 0,  0,  0,  R1[0][0],     R1[0][1],   R1[0][2]},
      {0, 0, 0, 0,  0,  0,  R1[1][0],     R1[1][1],   R1[1][2]},
      {0, 0, 0, 0,  0,  0,  R1[2][0],     R1[2][1],   R1[2][2]},
      {0, 0, 0, 0,  0,  0,  G2[0][0],     G2[0][1],   G2[0][2]},
      {0, 0, 0, 0,  0,  0,  G2[1][0],     G2[1][1],   G2[1][2]},
      {0, 0, 0, 0,  0,  0,  G2[2][0],     G2[2][1],   G2[2][2]},
    };

    mat9d FP;
    mat9d_mult(&F, &est->P, &FP);
    mat9d PFt;
    mat9d_mult_transpose(&est->P, &F, &PFt);

    mat9d_add(&FP, &PFt, &FP);

    /* add process noise */
    mat9d Qc = {{0}};
    for (int j=0; j<3; j++) {
      Qc[j+3][j+3] = est->q_acc*DT;
      Qc[j+6][j+6] = est->q_gyro*DT;
    } 
    mat9d_add(&FP, &Qc, &FP);

    mat9d_mult_scalar(&FP, MEKF_DT, &FP);
    mat9d_add(&est->P, &FP, &est->P);

    /* update reference attitude */
    rotd_assign_by_product(&est->state.R, &exp_delta);
  } /* for t < T */
}

void ekf_zoh_reset_predict(est_s *est, double DT, const vec3d *Ba, const vec3d *Bg)
{
  /* mean push */
  vec3d Ia;
  rotd_body2inertial(&est->state.R, Ba, &Ia);

  vec3d_add_s_mult(&est->state.pos, &est->state.vel, DT, &est->state.pos);
  vec3d_add_s_mult(&est->state.pos, &Ia, DT*DT/2, &est->state.pos);

  vec3d_add_s_mult(&est->state.vel, &Ia, DT, &est->state.vel);

  vec3d delta;
  vec3d_mult_scalar(Bg, DT, &delta);
  rotd rot_delta;
  rotd_from_rotation_vector(&delta, &rot_delta);
  /* update Reference attitude after the covariance push.. */

  /* cov push */
  mat3d acc_skew = {{0}};
  mat3d R1;         /* -Rref[acc x]dt */
  mat3d R2;         /* -Rref[acc x]dt^2/2 */
  mat3d Rref;
  mat3d Rdelta, RdeltaT;

  rotd_rotation_matrix(&rot_delta, &Rdelta);
  mat3d_transpose(&Rdelta, &RdeltaT);
  rotd_rotation_matrix(&est->state.R, &Rref);
  mat3d_skew(Ba, &acc_skew);
  mat3d_mult(&Rref, &acc_skew, &R1);
  mat3d_mult_scalar(&R1, -DT, &R1);
  mat3d_mult_scalar(&R1, DT/2, &R2);

  mat9d Ad = {
    {1, 0, 0, DT, 0,  0,  R2[0][0],     R2[0][1],   R2[0][2]},
    {0, 1, 0, 0,  DT, 0,  R2[1][0],     R2[1][1],   R2[1][2]},
    {0, 0, 1, 0,  0,  DT, R2[2][0],     R2[2][1],   R2[2][2]},
    {0, 0, 0, 1,  0,  0,  R1[0][0],     R1[0][1],   R1[0][2]},
    {0, 0, 0, 0,  1,  0,  R1[1][0],     R1[1][1],   R1[1][2]},
    {0, 0, 0, 0,  0,  1,  R1[2][0],     R1[2][1],   R1[2][2]},
    {0, 0, 0, 0,  0,  0,  RdeltaT[0][0],   RdeltaT[0][1],   RdeltaT[0][2]},
    {0, 0, 0, 0,  0,  0,  RdeltaT[1][0],   RdeltaT[1][1],   RdeltaT[1][2]},
    {0, 0, 0, 0,  0,  0,  RdeltaT[2][0],   RdeltaT[2][1],   RdeltaT[2][2]},  
  };

  mat9d AdP;
  mat9d_mult(&Ad, &est->P, &AdP);
  mat9d_mult_transpose(&AdP, &Ad, &est->P);

  mat3d Gamma, delta_skew, delta_outer, temp;
  mat3d_subtract(&RdeltaT, &mat3d_identity, &Gamma);
  mat3d_skew(&delta, &delta_skew);
  mat3d_outer(&delta, &delta, &delta_outer);
  mat3d_mult(&delta_skew, &Gamma, &temp);
  mat3d_add(&delta_outer, &temp, &Gamma);
  mat3d_div_scalar(&Gamma, vec3d_norm_sq(&delta), &Gamma);

  /* add process noise */
  mat9d GammaF =
    {
      {1, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 1, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 1, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 1, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 1, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 1, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, Gamma[0][0], Gamma[0][1], Gamma[0][2]},
      {0, 0, 0, 0, 0, 0, Gamma[1][0], Gamma[1][1], Gamma[1][2]},
      {0, 0, 0, 0, 0, 0, Gamma[2][0], Gamma[2][1], Gamma[2][2]},
    };

  mat9d temp2, temp3;
  mat9d Q = {{0}};
  for (int j=0; j<3; j++) {
    Q[j][j]     = squared(DT)*squared(DT)/4*est->q_acc;
    Q[j][j+3]   = Q[j+3][j] = squared(DT)*DT/2*est->q_acc;
    Q[j+3][j+3] = squared(DT)*est->q_acc;
    Q[j+6][j+6] = squared(DT)*est->q_gyro;
  }
  mat9d_mult(&GammaF, &Q, &temp2);
  mat9d_mult_transpose(&temp2, &GammaF, &temp3);

  mat9d_add(&est->P, &temp3, &est->P);

  /* update reference attitude */
  rotd_assign_by_product(&est->state.R, &rot_delta);
}

/****************************************************************************
 * UKFs 
 ****************************************************************************/

#ifdef UKFS

void ukf_zoh_reset_predict(est_s *est, double DT, const vec3d *Ba, const vec3d *Bg)
{
  int ret;

  /* generate the sigma points */

  /* create gsl matrix */
  gsl_matrix *P_aug = gsl_matrix_calloc(N, N);
  for (int i=0; i<9; i++) {
    for (int j=0; j<9; j++) { 
      gsl_matrix_set(P_aug, i, j, est->P[i][j]);
    }
  }

  if ( (ret = gsl_linalg_cholesky_decomp(P_aug)) !=  GSL_SUCCESS) {
    printf("[reset_ukf_predict] error in cholesky: %s\n", gsl_strerror(ret));
  }

  double sigma_points[2*N+1][N];

  for (int i=0; i<2*N+1; i++) {
    for (int j=0; j<3; j++) {
      sigma_points[i][j]   = est->state.pos[j];
      sigma_points[i][j+3] = est->state.vel[j];
      sigma_points[i][j+6] = 0;
    }
  }
  
  for (int i=0; i<N; i++) {
    for (int j=i; j<N; j++) {
      sigma_points[i+1][j]   += UKF_ALPHA*sqrt(N)*gsl_matrix_get(P_aug,j,i);
      sigma_points[i+1+N][j] -= UKF_ALPHA*sqrt(N)*gsl_matrix_get(P_aug,j,i);
    }
  }

  /* pass the sigma points through the propagation map */

  vec9d x_out[2*N+1];
  for (int i=0; i<2*N+1; i++) {
    vec3d *p = (vec3d *) &sigma_points[i][0];
    vec3d *v = (vec3d *) &sigma_points[i][3];
    vec3d *d = (vec3d *) &sigma_points[i][6];

    rotd exp_delta;
    rotd_from_rotation_vector(d, &exp_delta);
    rotd R;
    rotd_mult(&est->state.R, &exp_delta, &R);

    vec3d Ia;
    rotd_body2inertial(&R, Ba, &Ia);

    /* p[k+1] = p[k] + v[k]dt + a[k]dt^2/2 */
    vec3d_add_s_mult(p, v, DT, (vec3d *) &x_out[i][0]);
    vec3d_add_s_mult((vec3d *) &x_out[i][0], &Ia, DT*DT/2, (vec3d *) &x_out[i][0]);
    /* v[k+1] = v[k] + a[k]dt */
    vec3d_add_s_mult(v, &Ia, DT, (vec3d *) &x_out[i][3]);
    /* d[k+1] = log[exp(d[k])exp((w[k]+n[k])dt)] */
    vec3d wdeltatau;
    vec3d_mult_scalar(Bg, DT, &wdeltatau);

    rotd exp_wdeltatau;
    rotd_from_rotation_vector(&wdeltatau, &exp_wdeltatau);
    rotd rot_delta;
    rotd_mult(&exp_delta, &exp_wdeltatau, &rot_delta);
    rotd_normalize(&rot_delta);
    rotd_to_rotation_vector(&rot_delta, (vec3d *) &x_out[i][6]);
  }

  /* compute the statistics */

  vec9d mean = {0};
  for (int i=0; i<2*N+1; i++) {
    vec9d_add_s_mult(&mean, &x_out[i], 
      ((i==0) ? 1 - 1/squared(UKF_ALPHA) : 1.0/(2*(N))/squared(UKF_ALPHA) ) , 
      &mean);
  }

#ifdef __DEBUG__
  vec9d_print("mean = ", &mean);
#endif

  vec3d_copy( (vec3d *) &mean[0], &est->state.pos);
  vec3d_copy( (vec3d *) &mean[3], &est->state.vel);

  mat9d_initialize(&est->P, 0);

  for (int i=0; i<2*N+1; i++) {
    mat9d outer;
    vec9d a;
    vec9d_subtract(&x_out[i], &mean, &a);
    mat9d_outer(&a, &a, &outer);

    mat9d_mult_scalar(&outer, 
      (i==0) ? 1-1/squared(UKF_ALPHA) + 1-squared(UKF_ALPHA) + UKF_BETA : 1.0/(2*N)/squared(UKF_ALPHA), 
      &outer);

    mat9d_add(&est->P, &outer, &est->P);
  }

  /* add process noise */
  mat9d Q = {{0}};
  for (int j=0; j<3; j++) {
    Q[j][j]     = squared(DT)*squared(DT)/4*est->q_acc;
    Q[j][j+3]   = Q[j+3][j] = squared(DT)*DT/2*est->q_acc;
    Q[j+3][j+3] = squared(DT)*est->q_acc;
    Q[j+6][j+6] = squared(DT)*est->q_gyro;
  } 
  mat9d_add(&est->P, &Q, &est->P);

  /* attitude reset */
  reset_attitude_reset(est, (vec3d *) &mean[6]);
  gsl_matrix_free(P_aug);
}

void lg_ukf_predict(est_s *est, double DT, const vec3d *Ba, const vec3d *Bg)
{
  int ret;

  /* generate the sigma points */

  /* create gsl matrix, augmented for the nonlinear noise */
  gsl_matrix *P_aug = gsl_matrix_calloc(N, N);
  for (int i=0; i<9; i++) {
    for (int j=0; j<9; j++) { 
      gsl_matrix_set(P_aug, i, j, est->P[i][j]);
    }
  }

  if ( (ret = gsl_linalg_cholesky_decomp(P_aug)) !=  GSL_SUCCESS) {
    printf("[reset_ukf_predict] error in cholesky: %s\n", gsl_strerror(ret));
  }

  double sigma_points[2*N+1][N];

  for (int i=0; i<2*N+1; i++) {
    for (int j=0; j<3; j++) {
      sigma_points[i][j]   = est->state.pos[j];
      sigma_points[i][j+3] = est->state.vel[j];
      sigma_points[i][j+6] = 0;
    }
  }
  
  for (int i=0; i<N; i++) {
    for (int j=i; j<N; j++) {
      sigma_points[i+1][j]   += UKF_ALPHA*sqrt(N)*gsl_matrix_get(P_aug,j,i);
      sigma_points[i+1+N][j] -= UKF_ALPHA*sqrt(N)*gsl_matrix_get(P_aug,j,i);
    }
  }

  /* pass the sigma points through the propagation map */

  double x_out[2*N+1][6];
  rotd  R_out[2*N+1];
  for (int i=0; i<2*N+1; i++) {
    vec3d *p = (vec3d *) &sigma_points[i][0];
    vec3d *v = (vec3d *) &sigma_points[i][3];
    vec3d *d = (vec3d *) &sigma_points[i][6];

    rotd exp_delta;
    rotd_from_rotation_vector(d, &exp_delta);
    rotd R;
    // rotd_mult(&exp_delta, &est->state.R, &R); /* Kang uses different convention! */
    rotd_mult(&est->state.R, &exp_delta, &R); 

    vec3d Ia;
    rotd_body2inertial(&R, Ba, &Ia);

    /* p[k+1] = p[k] + v[k]dt + a[k]dt^2/2 */
    vec3d_add_s_mult(p, v, DT, (vec3d *) &x_out[i][0]);
    vec3d_add_s_mult((vec3d *) &x_out[i][0], &Ia, DT*DT/2, (vec3d *) &x_out[i][0]);
    /* v[k+1] = v[k] + a[k]dt */
    vec3d_add_s_mult(v, &Ia, DT, (vec3d *) &x_out[i][3]);
    
    /* R[k+1] = R[k] exp[(w[k]+n[k])dt] */
    vec3d wdeltatau;
    vec3d_mult_scalar(Bg, DT, &wdeltatau);
    rotd exp_wdeltatau;
    rotd_from_rotation_vector(&wdeltatau, &exp_wdeltatau);
    rotd_mult(&R, &exp_wdeltatau, &R_out[i]);
  }

  /* compute the statistics */

  double mean[6] = {0};
  for (int i=0; i<2*N+1; i++) {
    vec3d_add_s_mult((vec3d *) &mean[0], (vec3d *) &x_out[i][0], 
      ((i==0) ? 1 - 1/squared(UKF_ALPHA) : 1.0/(2*(N))/squared(UKF_ALPHA) ), 
      (vec3d *) &mean[0]);

    vec3d_add_s_mult((vec3d *) &mean[3], (vec3d *) &x_out[i][3], 
      ((i==0) ? 1 - 1/squared(UKF_ALPHA) : 1.0/(2*(N))/squared(UKF_ALPHA) ), 
      (vec3d *) &mean[3]);
  }
  vec3d_copy( (vec3d *) &mean[0], &est->state.pos);
  vec3d_copy( (vec3d *) &mean[3], &est->state.vel);

  /* Algorithm 1 Kang 2019 */
  rotd T;
  rotd_copy(&R_out[0], &T);
  for (int j=0; j<4; j++) {
    vec3d Lambda = {0};
    rotd T_inv;
    rotd_inverse(&T, &T_inv);
    for (int i=0; i<2*N+1; i++) {
      vec3d delta;
      rotd  Rdelta;

      rotd_mult(&T_inv, &R_out[i],  &Rdelta);
      rotd_to_rotation_vector(&Rdelta, &delta);

      vec3d_add_s_mult(&Lambda, &delta, 
        ((i==0) ? 1 - 1/squared(UKF_ALPHA) : 1.0/(2*(N))/squared(UKF_ALPHA) ),
         &Lambda);
    }

    rotd RLambda;
    rotd_from_rotation_vector(&Lambda, &RLambda);
    rotd_assign_by_product(&T, &RLambda);
  }

#ifdef __DEBUG__
  rotd Rinv;
  rotd_inverse(&est->state.R, &Rinv);
  rotd_assign_by_product(&Rinv, &T);
  vec3d temp;
  rotd_to_rotation_vector(&Rinv, &temp);
  vec3d_print("lg delta =", &temp);
#endif

  rotd_copy(&T, &est->state.R);

  vec9d q[2*N+1];
  rotd Rmean_inv;
  rotd_inverse(&est->state.R, &Rmean_inv);
  for (int i=0; i<2*N+1; i++) {
    vec3d_subtract((vec3d *) &x_out[i][0], (vec3d *) &mean[0], (vec3d *) &q[i][0]);
    vec3d_subtract((vec3d *) &x_out[i][3], (vec3d *) &mean[3], (vec3d *) &q[i][3]);

    rotd Rdelta;
    // rotd_mult(&R_out[i], &Rmean_inv, &Rdelta);
    rotd_mult(&Rmean_inv,  &R_out[i], &Rdelta);
    rotd_to_rotation_vector(&Rdelta, (vec3d *) &q[i][6]);
  }

  mat9d_initialize(&est->P, 0);
  for (int i=0; i<2*N+1; i++) {
    mat9d outer;
    mat9d_outer(&q[i], &q[i], &outer);
    mat9d_mult_scalar(&outer, 
      (i==0) ? 1-1/squared(UKF_ALPHA) + 1-squared(UKF_ALPHA) + UKF_BETA : 1.0/(2*N)/squared(UKF_ALPHA), 
      &outer);

    mat9d_add(&est->P, &outer, &est->P);
  }

  /* add process noise */
  mat9d Q = {{0}};
  for (int j=0; j<3; j++) {
    Q[j][j]     = squared(DT)*squared(DT)/4*est->q_acc;
    Q[j][j+3]   = Q[j+3][j] = squared(DT)*DT/2*est->q_acc;
    Q[j+3][j+3] = squared(DT)*est->q_acc;
    Q[j+6][j+6] = squared(DT)*est->q_gyro;
  } 
  mat9d_add(&est->P, &Q, &est->P);
  gsl_matrix_free(P_aug);
}

void usque_predict(est_s *est, double DT, const vec3d *Ba, const vec3d *Bg)
{
  int ret;

  /* generate the sigma points */

  /* create gsl matrix, augmented for the nonlinear noise */
  gsl_matrix *P_aug = gsl_matrix_calloc(N, N);
  for (int i=0; i<9; i++) {
    for (int j=0; j<9; j++) { 
      gsl_matrix_set(P_aug, i, j, est->P[i][j]);
    }
  }

  if ( (ret = gsl_linalg_cholesky_decomp(P_aug)) !=  GSL_SUCCESS) {
    printf("[reset_ukf_predict] error in cholesky: %s\n", gsl_strerror(ret));
  }

  double sigma_points[2*N+1][N];

  for (int i=0; i<2*N+1; i++) {
    for (int j=0; j<3; j++) {
      sigma_points[i][j]   = est->state.pos[j];
      sigma_points[i][j+3] = est->state.vel[j];
      sigma_points[i][j+6] = 0;
#if UKF_N == 12
      sigma_points[i][j+9] = 0;
#endif
    }
  }
  
  for (int i=0; i<N; i++) {
    for (int j=i; j<N; j++) {
      sigma_points[i+1][j]   += UKF_ALPHA*sqrt(N)*gsl_matrix_get(P_aug,j,i);
      sigma_points[i+1+N][j] -= UKF_ALPHA*sqrt(N)*gsl_matrix_get(P_aug,j,i);
    }
  }

  /* pass the sigma points through the propagation map */

  vec9d x_out[2*N+1];
  for (int i=0; i<2*N+1; i++) {
    vec3d *p = (vec3d *) &sigma_points[i][0];
    vec3d *v = (vec3d *) &sigma_points[i][3];
    vec3d *d = (vec3d *) &sigma_points[i][6];

    rotd exp_delta; 
    /* delta is now a generalized rodrigues parameter (GRP), choose a=0, f=2 */
    exp_delta[0] = 2*sqrt(4 + vec3d_norm_sq(d))/(4 + vec3d_norm_sq(d));
    vec3d_mult_scalar(d, exp_delta[0]/2, (vec3d *) &exp_delta[1]);
    rotd_normalize(&exp_delta);

    rotd R;
    rotd_mult(&est->state.R, &exp_delta, &R);

    vec3d Ia;
    rotd_body2inertial(&R, Ba, &Ia);

    /* p[k+1] = p[k] + v[k]dt + a[k]dt^2/2 */
    vec3d_add_s_mult(p, v, DT, (vec3d *) &x_out[i][0]);
    vec3d_add_s_mult((vec3d *) &x_out[i][0], &Ia, DT*DT/2, (vec3d *) &x_out[i][0]);
    
    /* v[k+1] = v[k] + a[k]dt */
    vec3d_add_s_mult(v, &Ia, DT, (vec3d *) &x_out[i][3]);
    
    /* eq(34) - (37) of 2003_Crassidis */
    vec3d wdeltatau;
    vec3d_mult_scalar(Bg, DT, &wdeltatau);
    rotd exp_wdeltatau;
    rotd_from_rotation_vector(&wdeltatau, &exp_wdeltatau);
    
    rotd exp_delta_exp_wdt;
    rotd_mult(&exp_delta, &exp_wdeltatau, &exp_delta_exp_wdt);
    
    rotd exp_wdt_inv;
    rotd_inverse(&exp_wdeltatau, &exp_wdt_inv);

    rotd delta_rot;
    rotd_mult(&exp_wdt_inv, &exp_delta_exp_wdt, &delta_rot);    
    vec3d_mult_scalar((vec3d *) &delta_rot[1], 2/delta_rot[0], (vec3d *) &x_out[i][6]);
  }

  /* compute the statistics */

  vec9d mean = {0};
  for (int i=0; i<2*N+1; i++) {
    vec9d_add_s_mult(&mean, &x_out[i], 
      ((i==0) ? 1 - 1/squared(UKF_ALPHA) : 1.0/(2*(N))/squared(UKF_ALPHA) ) , 
      &mean);
  }
  vec3d_copy( (vec3d *) &mean[0], &est->state.pos);
  vec3d_copy( (vec3d *) &mean[3], &est->state.vel);
  /* usqueu's strange thing is that only the central sigma point is used as the ref */
  vec3d wdt;
  rotd exp_wdt;
  vec3d_mult_scalar(Bg, DT, &wdt);
  rotd_from_rotation_vector(&wdt, &exp_wdt);
  rotd_assign_by_product(&est->state.R, &exp_wdt);

  mat9d_initialize(&est->P, 0);

  for (int i=0; i<2*N+1; i++) {
    mat9d outer;
    vec9d a;
    vec9d_subtract(&x_out[i], &mean, &a);
    mat9d_outer(&a, &a, &outer);

    mat9d_mult_scalar(&outer, 
      (i==0) ? 1-1/squared(UKF_ALPHA) + 1-squared(UKF_ALPHA) + UKF_BETA : 1.0/(2*N)/squared(UKF_ALPHA), 
      &outer);

    mat9d_add(&est->P, &outer, &est->P);
  }

  /* add process noise */
  mat9d Q = {{0}};
  for (int j=0; j<3; j++) {
    Q[j][j]     = squared(DT)*squared(DT)/4*est->q_acc;
    Q[j][j+3]   = Q[j+3][j] = squared(DT)*DT/2*est->q_acc;
    Q[j+3][j+3] = squared(DT)*est->q_acc;
    Q[j+6][j+6] = squared(DT)*est->q_gyro;
  } 
  mat9d_add(&est->P, &Q, &est->P);
  gsl_matrix_free(P_aug);
}

#endif /* ifdef UKFS */