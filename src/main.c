/****************************************************************************
  main.c
  Copyright (C) 2019  Rajan Gill

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/
/**
 * simulate rigid body the manuscript
 * run estimators: reset-ekf full order, and MEKF
 * run the estimators for different euler dt's for comparison 
 * log results to file
 */

#include "estimators.h"

#include <math.h>
#include <time.h>

#include <vectord.h>
#include <logger.h>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_odeiv2.h>  /* use GSL for solving the ODE */
#include <gsl/gsl_rng.h>     /* random number generator */
#include <gsl/gsl_randist.h>

/****************************************************************************
 * Preprocessor 
 ****************************************************************************/

#define TMAX            100
#define SIM_DT          0.001  /* how much time until the next sample of sim state,
                                * will also log at this rate
                                */
#define MEAS_DT         0.1

#define R_POS           100
#define GYRO_VAR        0.01
#define ACC_VAR         0
#define INIT_COV        1
#define INIT_ANGLE_COV  0.1
//#define SAMPLE_INITIAL /* can't really do, since usque has a different meaning for angle cov */
// #define LOG_CORRECTIONS_ONLY

/****************************************************************************
 * Private Types
 ****************************************************************************/

typedef union {
  double x[10]; 
  struct {
    vec3d pos;
    vec3d vel;
    rotd  rot;
  };
} ode_state_u;

typedef struct {
  ode_state_u   state;
  double        t;
} sim_state_s;

/****************************************************************************
 * Private Data
 ****************************************************************************/

static sim_state_s sim_state = 
  {
    .t = 0,
#ifndef SAMPLE_INITIAL    
    .state = { 
               .pos={100,100,100},
               .vel={10,10,10},
               .rot={cos(M_PI_2/2), 0, 0, sin(M_PI_2/2)},
             },
#endif
  };

/****************************************************************************
 * Private Function Definitions
 ****************************************************************************/

#define PBSTR "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"
#define PBWIDTH 60

/**
 * https://stackoverflow.com/questions/14539867/how-to-display-a-progress-indicator-in-pure-c-c-cout-printf/14539953
 */
static void print_progress(double fraction)
{
  int val = (int) (fraction * 100);
  int lpad = (int) (fraction * PBWIDTH);
  int rpad = PBWIDTH - lpad;
  printf ("\r%3d%% [%.*s%*s]", val, lpad, PBSTR, rpad, "");
  fflush (stdout);
}

/**
 * define the body frame acc and w as a function of time
 */
static void imu(double t, vec3d *Ba, vec3d *Bg)
{
  (*Ba)[0] = 1*fabs(cos(t*1));
  (*Ba)[1] = 10*fabs(sin(t*1));
  (*Ba)[2] = 100*fabs(cos(t*1));

  (*Bg)[0] = 10*fabs(sin(t*1));
  (*Bg)[1] = 1*fabs(cos(t*1));
  (*Bg)[2] = 1*fabs(sin(t*1));
}

/**
 * simple dynamics
 */
static int dynamics(double t, const double x[], double xd[], void *params)
{
  ode_state_u *state = (ode_state_u *) x;
  ode_state_u *stateD = (ode_state_u *) xd;

  vec3d Ba, Bg;
  imu(t, &Ba, &Bg);

  /* pD = v */
  vec3d_copy(&state->vel, &stateD->pos);

  /* vD = Ra */
  rotd_body2inertial(&state->rot, &Ba, &stateD->vel);

  /* qD = Q(q)w */
  rotd_rate(&state->rot, &Bg, &stateD->rot);
  return GSL_SUCCESS;
}

/****************************************************************************
 * Public Function Definitions
 ****************************************************************************/

/**
 * main
 */
int main(int argc, char *argv[])
{
  /* initialize sim */
  gsl_odeiv2_system ode_sys = {
    .function  = dynamics,
    .jacobian  = NULL,
    .dimension = 10,
    .params    = NULL,
  };

  gsl_odeiv2_driver *ode_drv = gsl_odeiv2_driver_alloc_y_new(&ode_sys, 
    gsl_odeiv2_step_rk8pd, 1e-6, 1e-6, 1e-6);

  gsl_rng *rng = gsl_rng_alloc(gsl_rng_taus);
  struct timespec tp;
  clock_gettime(CLOCK_REALTIME, &tp);
  gsl_rng_set(rng, tp.tv_nsec + tp.tv_sec);

  /* initialize ests */
  est_s ests[] = {
#ifndef UKFS
    {
      .state          = {.pos = {0}, .vel = {0}, .R = {1,0,0,0}},
      .predict        = ekf_euler_reset_predict,
      .gps_update     = gps_update,
      .Gamma          = first_order_gamma,
      .attitude_reset = reset_attitude_reset,
    },
    {
      .state          = {.pos = {0}, .vel = {0}, .R = {1,0,0,0}},
      .predict        = ekf_euler_reset_1_predict,
      .gps_update     = gps_update,
      .Gamma          = zero_order_gamma,
      .attitude_reset = reset_attitude_reset,
    },
    {
      .state          = {.pos = {0}, .vel = {0}, .R = {1,0,0,0}},
      .predict        = ekf_zoh_reset_predict,
      .gps_update     = gps_update,
      .Gamma          = full_order_gamma,
      .attitude_reset = reset_attitude_reset,
    },
    {
      .state          = {.pos = {0}, .vel = {0}, .R = {1,0,0,0}},
      .predict        = mekf_predict,
      .gps_update     = gps_update,
      .Gamma          = zero_order_gamma,
      .attitude_reset = mekf_attitude_reset,
    },
    {
      .state          = {.pos = {0}, .vel = {0}, .R = {1,0,0,0}},
      .predict        = mekf_predict,
      .gps_update     = gps_update,
      .Gamma          = first_order_gamma,
      .attitude_reset = mekf_attitude_reset,
    },
    {
      .state          = {.pos = {0}, .vel = {0}, .R = {1,0,0,0}},
      .predict        = mekf_predict,
      .gps_update     = gps_update,
      .Gamma          = full_order_gamma,
      .attitude_reset = mekf_attitude_reset,
    },
    {
      .state          = {.pos = {0}, .vel = {0}, .R = {1,0,0,0}},
      .predict        = mekf_predict,
      .gps_update     = gps_update,
      .Gamma          = full_order_gamma_gibbs,
      .attitude_reset = mekf_attitude_reset,
    },
    {
      .state          = {.pos = {0}, .vel = {0}, .R = {1,0,0,0}},
      .predict        = mekf_predict,
      .gps_update     = gps_update,
      .Gamma          = full_order_gamma_quaternion,
      .attitude_reset = mekf_attitude_reset,
    },
#else    
    {
      .state          = {.pos = {0}, .vel = {0}, .R = {1,0,0,0}},
      .predict        = ekf_zoh_reset_predict,
      .gps_update     = gps_update,
      .Gamma          = full_order_gamma,
      .attitude_reset = reset_attitude_reset,
    },
    {
      .state          = {.pos = {0}, .vel = {0}, .R = {1,0,0,0}},
      .predict        = ukf_zoh_reset_predict,
      .gps_update     = gps_update,
      .Gamma          = full_order_gamma,
      .attitude_reset = reset_attitude_reset,
    },
    {
      .state          = {.pos = {0}, .vel = {0}, .R = {1,0,0,0}},
      .predict        = lg_ukf_predict,
      .gps_update     = gps_update,
      .Gamma          = full_order_gamma,
      .attitude_reset = reset_attitude_reset,
    },
    {
      .state          = {.pos = {0}, .vel = {0}, .R = {1,0,0,0}},
      .predict        = usque_predict,
      .gps_update     = gps_update,
      .Gamma          = zero_order_gamma,
      .attitude_reset = usque_attitude_reset,
    },
    {
      .state          = {.pos = {0}, .vel = {0}, .R = {1,0,0,0}},
      .predict        = usque_predict,
      .gps_update     = gps_update,
      .Gamma          = full_order_gamma_gibbs,
      .attitude_reset = usque_attitude_reset,
    },
#endif
  };

  int NUM_ESTS = sizeof(ests)/sizeof(est_s);
  for (int i=0; i<NUM_ESTS; i++) {
    mat9d_diag(&ests[i].P, INIT_COV);
    
    for (int j=0; j<3; j++) {
      ests[i].P[j+6][j+6] = INIT_ANGLE_COV;
    }
    ests[i].q_acc  = ACC_VAR;
    ests[i].q_gyro = GYRO_VAR;
    ests[i].r      = R_POS;
  }

  log_init((NUM_ESTS)*(3)); 

  /* initialize state */
#ifdef SAMPLE_INITIAL    
  vec3d temp;
  for (int i=0; i<3; i++) {
    sim_state.state.pos[i] = gsl_ran_gaussian(rng, sqrt(INIT_COV));
    sim_state.state.vel[i] = gsl_ran_gaussian(rng, sqrt(INIT_COV));
    temp[i] = gsl_ran_gaussian(rng, sqrt(INIT_ANGLE_COV));
  }
  rotd_from_rotation_vector(&temp, &sim_state.state.rot);
#endif  

  /* run sim and ests, log at some reasonable rate */
  while (sim_state.t < TMAX) {
    if (gsl_odeiv2_driver_apply(ode_drv, 
        &sim_state.t, 
        sim_state.t + SIM_DT, 
        sim_state.state.x) != GSL_SUCCESS) {
      printf("gsl integration failed\n");
      goto exit;
    }
    /* normalize rot */
    rotd_normalize(&sim_state.state.rot);

    /* measurements and run estimators */
    vec3d Ba, Bg;
    imu(sim_state.t - SIM_DT, &Ba, &Bg);
    for (int i=0; i<3; i++) {
      Ba[i] += gsl_ran_gaussian(rng, sqrt(ACC_VAR));
      Bg[i] += gsl_ran_gaussian(rng, sqrt(GYRO_VAR));
    }

    for (int i=0; i<NUM_ESTS; i++) {
      ests[i].predict(&ests[i], SIM_DT, &Ba, &Bg);
    }

    float log[(NUM_ESTS)][3];

    if ( fmod(sim_state.t, MEAS_DT) <= SIM_DT && sim_state.t > SIM_DT*2) {
      vec3d p;
      for (int i=0; i<3; i++) {
        p[i] = sim_state.state.pos[i] + gsl_ran_gaussian(rng, sqrt(R_POS));
      }
      for (int i=0; i<NUM_ESTS; i++) {
        vec9d corrections;
        ests[i].gps_update(&ests[i], &p, &corrections);
#ifdef LOG_CORRECTIONS_ONLY
        log[i][0] = vec3d_norm((vec3d *) &corrections[0]);
        log[i][1] = vec3d_norm((vec3d *) &corrections[3]);
        log[i][2] = vec3d_norm((vec3d *) &corrections[6]);
#endif
      }
#ifdef LOG_CORRECTIONS_ONLY
      log_push(sim_state.t*1e6, (const float *) log);
#endif
    }

    /* log */
#ifndef LOG_CORRECTIONS_ONLY
    for (int i=0; i<NUM_ESTS; i++) {
      vec3d e;
      vec3d_subtract(&sim_state.state.pos, &ests[i].state.pos, &e);
      log[i][0] = vec3d_norm(&e);

      vec3d_subtract(&sim_state.state.vel, &ests[i].state.vel, &e);
      log[i][1] = vec3d_norm(&e);

      rotd errorR, simR_inv;
      rotd_inverse(&sim_state.state.rot, &simR_inv);
      rotd_mult(&simR_inv, &ests[i].state.R, &errorR);
      vec3d v;
      rotd_to_rotation_vector(&errorR, &v);
      log[i][2] = (isnan(log[i][0])) ? NAN : vec3d_norm(&v);
    }
    log_push(sim_state.t*1e6, (const float *) log);
#endif
    print_progress(sim_state.t/TMAX);
  } /* while t < TMAX */

exit:
  log_deinit();
  gsl_rng_free(rng);
  gsl_odeiv2_driver_free(ode_drv);  
  return 0;
}
