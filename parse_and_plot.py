"""
parse_and_plot.py: self-contained parser and plotter
Copyright (C) 2019  Rajan Gill

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import numpy as np
import matplotlib.pylab as plt
import os

## PARAMETERS
UKF = False
log_folder = "log/"

if UKF is False:
    label = ["ekf-euler-reset-1", "ekf-euler-reset-0", "ekf-zoh-reset-F", "mekf-0", "mekf-1", "mekf-F", "mekf-G", "mekf-qv"]
else:
    label = ["ekf-zoh-reset-F", "ukf-zoh-reset-F", "lg-ukf", "usque-0", "usque-G"]


class LogParser:
    def __init__(self, file, no_floats):
        """

        :param file:        relative or abs path to file
        :param no_floats:   number of floats, format is [systime_t] [float1] [float2] ...
        """

        self.file = file
        self.no_floats = no_floats


    def parse(self):
        """

        :return: t (N,) , data (N,no_floats) , N is number of data entries in the log
        """

        print("Parsing log: ", self.file, "No of floats = ", self.no_floats)

        data = np.fromfile(self.file, dtype=np.float32)
        data = np.reshape(data, ( int(len(data)/(1+self.no_floats)), 1+self.no_floats) )

        t = data[:,0].view('u4') / 1e6 # convert to seconds
        floats = data[:,1:]

        print("Parsing complete")
        return t, floats


fig, ax = plt.subplots(3,1,sharex=True)

files = os.listdir(log_folder)
files.sort()
# get length
t, floats = LogParser(log_folder + "log_01.dat", 3 * len(label)).parse()
Floats = np.zeros((len(t), 3 * len(label), len(files)))

for i, file in enumerate(files):
    t, floats = LogParser(log_folder + file, 3 * len(label)).parse()
    Floats[:, :, i] = floats

for i in range(len(label)):
    for j in range(3):
        ax[j].plot(t, np.percentile(Floats[:, 3 * i + j, :], 50, axis=1), label=label[i])
        ax[j].fill_between(t,
                           np.percentile(Floats[:, 3 * i + j, :], 25, axis=1),
                           np.percentile(Floats[:, 3 * i + j, :], 75, axis=1), alpha=0.4,
                           rasterized=True)
for i in range(2):
    ax[i].set_yscale("log")

ax[0].legend()
ax[0].set_ylabel("$||\hat{p}_{k|k} - p(t)||$ [m]")
ax[1].set_ylabel("$||\hat{v}_{k|k} - v(t)||$ [m/s]")
ax[2].set_ylabel("$||\log(R(t)^\\top R^\\mathrm{ref}_{k})||$ [rad]")
ax[2].set_xlabel("$t [s]$")

plt.show()
