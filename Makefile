# ========================================================================== #
# Copyright (C) 2019  Rajan Gill
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
# ========================================================================== #

DEBUG = 0

# ========================================================================== #
# = Compiler settings
# ========================================================================== #

CC = gcc
CFLAGS = -Wall

#includes 
CFLAGS += -I inc/
# opts
ifeq ($(DEBUG), 1)
	CFLAGS += -g -O0 -D__DEBUG__
else
	CFLAGS += -O3 -march=native
endif

# ========================================================================== #
# = Linker settings
# ========================================================================== #

LFLAGS =  -lm\
					-lgsl\
				 	-lopenblas\
				 	
# ========================================================================== #
# = Object Files
# ========================================================================== #

CSRCS  = $(wildcard src/*.c)
OBJS   = $(patsubst %.c,%.o,$(wildcard src/*.c))
BIN    = main.out

# ========================================================================== #
# = Targets
# ========================================================================== #

default: $(BIN)

%.out: $(OBJS)
	$(CC) $^ -o $@ $(LFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@


clean:
	rm -rf *.o 
	rm -rf $(BIN)