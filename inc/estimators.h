/****************************************************************************
  estimators.h
  Copyright (C) 2019  Rajan Gill

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include <vectord.h>

VECTOR_DEFINE_COMMON_VEC(9,d,double)
VECTOR_DEFINE_COMMON_MAT(9,d,double)

/****************************************************************************
 * Preprocessor 
 ****************************************************************************/

// #define UKFS /* enable or disable UKFs */

/****************************************************************************
 * Public Types
 ****************************************************************************/

typedef struct {
	/* mean */
	struct {
		vec3d pos;
		vec3d vel;
		rotd R;
	} state;
	
	/* var */
	mat9d P;
	double r; /* gps pos noise var */
	double q_acc, q_gyro; /* acc and gyro noise */

	/* function ptrs */
	void (*predict)();
	void (*gps_update)();
	void (*Gamma)();
	void (*attitude_reset)(); /* separate than Gamma, as depending on filter there is 
														 * a different interpretation to the delta
														 */
} est_s;

/****************************************************************************
 * Public Function Declarations
 ****************************************************************************/

void zero_order_gamma(const vec3d *v, mat3d *Gamma);
void first_order_gamma(const vec3d *v, mat3d *Gamma);
void full_order_gamma(const vec3d *v, mat3d *Gamma);
void full_order_gamma_gibbs(const vec3d *v, mat3d *Gamma);
void full_order_gamma_quaternion(const vec3d *v, mat3d *Gamma);

void gps_update(est_s *est, const vec3d *p, vec9d *corrections);

void mekf_predict(est_s *est, double delta_t, const vec3d *a, const vec3d *g);
void ekf_euler_reset_predict(est_s *est, double delta_t, const vec3d *a, const vec3d *g);
void ekf_euler_reset_1_predict(est_s *est, double delta_t, const vec3d *a, const vec3d *g);
void ekf_zoh_reset_predict(est_s *est, double delta_t, const vec3d *a, const vec3d *g);

#ifdef UKFS
void ukf_zoh_reset_predict(est_s *est, double delta_t, const vec3d *Ba, const vec3d *Bg);
void lg_ukf_predict(est_s *est, double delta_t, const vec3d *Ba, const vec3d *Bg);
void usque_predict(est_s *est, double delta_t, const vec3d *Ba, const vec3d *Bg);
#endif

void reset_attitude_reset(est_s *est, const vec3d *d);
void mekf_attitude_reset(est_s *est, const vec3d *d);
void usque_attitude_reset(est_s *est, const vec3d *d);