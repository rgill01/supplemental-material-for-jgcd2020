/****************************************************************************
	Spawn a low priority logging thread that writes to the hard disk floats it 
	receives.

	Copyright (C) 2017  Rajan Gill

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

/* TODO
 * create a log "instance", i.e. allow multiple loggers
 */

#ifndef __INCLUDE_LIB_LOGGER_H_
#define __INCLUDE_LIB_LOGGER_H_

#include <stdint.h>

/****************************************************************************
 * Public Types
 ****************************************************************************/

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * log_push
 *
 *  Description:
 *  	Push the t and float array to the log file. Can call from the same 
 *    thread group as the one that called log_init.
 *      
 ****************************************************************************/

int log_push(uint32_t t, const float *f);

/****************************************************************************
 * log_init
 *
 *  Description:
 *  	initialize logger
 *      
 ****************************************************************************/

int log_init(uint16_t no_of_floats);

/****************************************************************************
 * log_deinit
 *
 *  Description:
 *  	destroy log thread
 *      
 ****************************************************************************/

void log_deinit(void);

#endif /* include guard */