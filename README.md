# Supplementary Material for manuscript: ["A Full-order Solution to the Attitude Reset Problem for Kalman Filtering of Attitudes"](https://arc.aiaa.org/doi/10.2514/1.G004134)

## Paper details

- Title: "A Full-order Solution to the Attitude Reset Problem for Kalman Filtering of Attitudes"
- Authors: Rajan Gill, Mark W. Mueller and Raffaello D’Andrea
- Journal: AIAA Journal of Guidance, Control, and Dynamics
- DOI: 10.2514/1.G004134


## Description
Self-contained code for the simulations in Section V of the paper. This has only been tested on a linux machine (Solus 4.0).

## Dependencies 
- [gsl](https://www.gnu.org/software/gsl/)
- gcc
- python 3

## Build instructions
In the root directory run:

```
make
```

## Usage
To run and plot:

```
./run <N>
python3 parse_and_plot.py
```
where N is the number of runs.

By default, UKFs are disabled. To enable, uncomment line 28 in inc/estimators.h, and set the variable UKF to True on line 12 in parse_and_plot.py. Also, by default, $\Delta\tau_\mathrm{mekf} = 0.001$. This value can be adjusted by changing line 30 in src/estimator.c.

## License
[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)
